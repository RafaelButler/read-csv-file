export interface CreateUserDto {
    name: string;
    city: string;
    country: string;
    favorite_sport: string
}