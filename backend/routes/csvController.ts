import {Request, response, Response} from "express";
import {CreateUserUseCase} from "../use-cases/createUserUseCase";
import csvParser from "csv-parser";
import {CreateUserDto} from "../dto/createUserDto";
import {GetQueryUserUseCase} from "../use-cases/getQueryUserUseCase";
import {GetUserUseCase} from "../use-cases/getUserUseCase";

const userCase = new GetUserUseCase()

export class CsvController {
    static async store(req: Request, res: Response) {
        const csvData = req.file?.buffer.toString();

        if (!csvData) {
            return res.status(400).json({error: 'Arquivo CSV não encontrado'});
        }

        const userUseCase = new CreateUserUseCase();
        const results: any[] = [];

        csvParser().on('data', (data) => {
            results.push(data);
        }).on('end', async () => {
            try {

                for (const data of results) {
                    await userUseCase.execute(data);
                }

                const users = await userCase.execute()

                return res.json(users).status(201);
            } catch (e) {
                res.status(500).json({error: 'Erro ao importar dados'});
            }
        }).end(csvData)
    }
    
    static async show(req: Request, res: Response) {
        const query = req.query.q as string;
        const userQueryCase = new GetQueryUserUseCase()

        if (query) {
            const users = await userQueryCase.execute(query)
            return res.json(users).status(200);
        }
        const users = await userCase.execute();

        return res.json(users).status(200);
    }
}

