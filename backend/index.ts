import express from 'express';
import cors from 'cors';
import multer from 'multer';
import {CsvController} from "./routes/csvController";

const app = express();
const port = 3000;
app.use(cors())
const upload = multer();
app.use(express.json());

app.post('/api/files', upload.single('data'), CsvController.store);
app.get('/api/users', CsvController.show);

app.listen(port, () => {
    console.log("Server up")
})

