import {prisma} from "../prisma/prismaClient";
import {User} from "@prisma/client";

export class GetUserUseCase{
    async execute(): Promise<User[]>{
        return prisma.user.findMany();
    }
}