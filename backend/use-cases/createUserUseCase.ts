import {CreateUserDto} from "../dto/createUserDto";
import {prisma} from "../prisma/prismaClient";
import {User} from "@prisma/client";

export class CreateUserUseCase {
    async execute(data: CreateUserDto): Promise<User> {
        return prisma.user.create({
            data
        });
    }
}