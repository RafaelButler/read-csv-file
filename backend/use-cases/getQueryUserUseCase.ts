import {prisma} from "../prisma/prismaClient";
import {User} from "@prisma/client";

export class GetQueryUserUseCase {
    async execute(query: string): Promise<User[]> {
        return prisma.user.findMany({
            where: {
                OR: [
                    {name: {contains: query}},
                    {city: {contains: query}},
                    {country: {contains: query}},
                    {favorite_sport: {contains: query}}
                ]
            }
        });
    }
}