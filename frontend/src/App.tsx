import './App.css'
import {useEffect, useState} from "react";
import axios from "axios";
import {Card} from "./components/card.tsx";
import {FormCsv} from "./components/form.tsx";
import {Listing} from "./components/listing.tsx";
import {Filter} from "./components/filter.tsx";
import {UserProvider} from "./context/userContext.tsx";

function App() {


    return (
        <UserProvider>
            <div className="flex mb-4">
                <section className="text-gray-600 body-font">
                    <div className="container px-5 py-24 mx-auto">

                        <div className="flex items-center justify-center w-full mb-4">
                            <div className="mb-3">
                                <FormCsv/>
                            </div>
                        </div>

                        <div className="border border-b border border-gray-600 mb-4"></div>


                        {/*<h3 className="mb-4 text-gray-100">Listagem de Usuarios</h3>*/}
                        <Filter/>
                        <div className="flex flex-wrap w-[100vh] -m-2">
                            <Listing/>
                        </div>
                    </div>
                </section>
            </div>
        </UserProvider>
    )
}

export default App
