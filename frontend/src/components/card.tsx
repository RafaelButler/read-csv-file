interface Props {
    name: string;
    city: string;
    country: string;
    favorite_sport: string

}

export function Card(props: Props){
    return(
        <div className="p-2 lg:w-1/3 md:w-1/2 w-full">
                <div className="h-full flex items-center text-left bg-neutral-700 border-neutral-600 border p-4 rounded-lg">
                <svg  fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" className="w-6 h-6 text-gray-200" viewBox="0 0 24 24">
                    <path d="M20 21v-2a4 4 0 00-4-4H8a4 4 0 00-4 4v2"></path>
                    <circle cx="12" cy="7" r="4"></circle>
                </svg>
                <div className="flex-grow ml-4">
                    <h2 className="text-gray-100 title-font font-medium">{props.name}</h2>
                    <p className="text-gray-400">{props.city}</p>
                </div>
            </div>
        </div>
    )
}