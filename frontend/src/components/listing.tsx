import {Card} from "./card.tsx";
import {useContext, useEffect, useState} from "react";
import axios from "axios";
import {api} from "../service/api.ts";
import {UserContext} from "../context/userContext.tsx";

export function Listing() {
    const {users, setUsers} = useContext(UserContext);
    
    const get = async () => {
        const res = await api.get('users');
        setUsers(res.data)
    }
    useEffect(() => {
        get()
    }, [])

    return (
        users.length > 0 && users.map(user => (
            <Card key={user.id} name={user.name} city={user.city} country={user.country}
                  favorite_sport={user.favorite_sport}/>
        ))
    )
}