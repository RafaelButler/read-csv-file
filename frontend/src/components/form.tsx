import axios from "axios";
import {useContext, useState} from "react";
import {api} from "../service/api.ts";
import {UserContext} from "../context/userContext.tsx";

export function FormCsv(props: any) {
    const {setUsers} = useContext(UserContext);
    const [file, setFile] = useState(null);

    const handleFileChange = (e) => {
        setFile(e.target.files[0]);
    };

    const handleUpload = async () => {
        if (!file) {
            return;
        }

        const formData = new FormData();
        formData.append('data', file);

        try {
            const response = await api.post('/files', formData, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                },
            });

            setUsers(response.data)
        } catch (error) {
            console.error(error);
        }
    };

    return (
        <>
            <label form="formFile" className="mb-2 inline-block text-neutral-700 dark:text-neutral-200">Inserir arquivo
                CSV*</label>
            <div className="flex items-center">
                <input
                    className="relative m-0 block w-full min-w-0 flex-auto rounded border border-solid border-neutral-300 bg-clip-padding px-3 py-[0.32rem] text-base font-normal text-neutral-700 transition duration-300 ease-in-out file:-mx-3 file:-my-[0.32rem] file:overflow-hidden file:rounded-none file:border-0 file:border-solid file:border-inherit file:bg-neutral-100 file:px-3 file:py-[0.32rem] file:text-neutral-700 file:transition file:duration-150 file:ease-in-out file:[border-inline-end-width:1px] file:[margin-inline-end:0.75rem] hover:file:bg-neutral-200 focus:border-primary focus:text-neutral-700 focus:shadow-te-primary focus:outline-none dark:border-neutral-600 dark:text-neutral-200 dark:file:bg-neutral-700 dark:file:text-neutral-100 dark:focus:border-primary"
                    type="file"
                    onChange={handleFileChange}
                    id="formFile"
                />

                <button className="w-40 bg-neutral-700 text-gray-300 ml-4 border-gray-400 focus:border-gray-400"
                        onClick={handleUpload}>Salvar
                </button>
            </div>
        </>
    )
}