import {api} from "../service/api.ts";
import {useContext} from "react";
import {UserContext} from "../context/userContext.tsx";

export function Filter() {

    const {setUsers, users} = useContext(UserContext);

    const handleSearch = async (event) => {
        const response = await api.get(`/users?q=${event.target.value}`);
        setUsers(response.data)
    }
    return (
        <div className="text-right">
            <input id="filter" name="fillter" type="text" placeholder="Filter"
                   onChange={handleSearch}
                   autoComplete="filter"
                   className="block mb-4 rounded-sm border-0 px-3  py-1.5 text-gray-200 shadow-sm  placeholder:text-gray-400 focus sm:text-sm sm:leading-6"/>
        </div>

    )

}
